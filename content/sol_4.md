+++
title = "Sistemas de Inspección 100%: Control de calidad integral"
description = ""
keywords = ["Soluciones","Inspección","Superficie","TubeScan"]
+++

<div style="overflow: hidden; width: 100%">

<div style="float: left; min-width: 30%; max-width: 50%;">
<img src="/img/web-inspection-sol2-01.jpg" style="width: 100%; padding: 0 20px 0 0;">
</div>

Los proveedores de las industrias de procesamiento de bandas se encuentran bajo presión constante. Mientras que los requisitos con relación a la calidad aumentan constantemente, la velocidad de producción también ha crecido de forma significativa en los últimos años. Los fabricantes deben ofrecer **resultados fiables y constantes** a precios cada vez más reducidos y con las mayores velocidades de banda.

**El resultado**: Los tiempos de preparación y cambio y el uso del material deben reducirse de manera efectiva y el material de descarte debe evitarse casi por completo. Cualquier fabricante que quiera mantenerse en el mercado necesita tecnología de primera clase para obtener resultados perfectos hasta el último detalle.

**Control de calidad competente:** Los sistemas de inspección de BST monitorizan la impresión de manera sencilla, integral y eficien
te para obtener **productos de impresión de primera clase**. Los sistemas de inspección 100 % utilizan una o más cámaras lineales y luz constante. Todo el ancho de banda se captura línea a línea y se muestra en la pantalla como patrón completo. En este punto, existe la opción del zoom digital.

</div>
<br><br>

<div style="overflow: hidden; width: 100%">

<h3><p style="color: mediumblue;">Gama TubeScan</p></h3>

<div style="float: right; min-width: 30%; max-width: 50%;">
<img src="/img/web-inspection-sol2-03.jpg" style="width:100%; padding: 0 0 0 20px;">
</div>

La gama **TubeScan** de BST pone el listón alto en lo relativo al control de calidad durante la impresión y el montaje. El sistema combina tecnología de la imagen inteligente con el principio estroboscópico y ofrece una solución efectiva y eficiente para la inspección 100 % de la impresión y las vistas detalladas.

Ahora podrá cubrir todas sus necesidades con un solo sistema modular, desde la sencilla monitorización de banda 100 % y la detección de falta de etiquetas y los residuos matrices hasta la inspección 100 % de la impresión de alta resolución dentro de un determinado flujo de trabajo.

</div>
<br><br>

<h4><p style="color: mediumblue;">TubeScan XL</p></h4>

<div style="overflow: hidden; width: 100%">

<div style="float: left; min-width: 30%; max-width: 50%;">
<img src="/img/web-inspection-sol2-08.jpg" style="width: 100%; padding: 0 20px 50px 0;">
</div>

El exitoso sistema modular de inspección estroboscopica ahora está disponible para aplicaciones de banda ancha.

La serie XL cubre anchos de banda entre 900mm y 1700mm, y puede ser equipado con el rango completo de accesorios para la serie TubeScan; opciones de iluminación de contorno, retroiluminación, o iluminación ultra violeta.

Está disponible en dos configuraciones:

* <h5><p style="color: mediumblue;">TubeScan XL para imprentas</p></h5>
Optimizado para su montaje en la etapa de impresión, configurado para una alta resolución de pixel, asegura máxima calidad del material impreso, minimizando los desechos.

* <h5><p style="color: mediumblue;">TubeScan XL para cortadoras</p></h5>
Configurado para el mejor desempeño en la inspección es ideal para cortadoras rebobinadoras; permite inspección en velocidades hasta 800 m/min; asegura la mejor calidad en la impresión evitando costosos reclamos de los clientes.

</div>
<br><br>

<div style="overflow: hidden; width: 100%">

<h3><p style="color: mediumblue;">iPQ-Check</p></h3>

<div style="float: right; min-width: 30%; max-width: 50%;">
<img src="/img/web-inspection-sol2-02.jpg" style="width: 100%; padding: 0 0 0 20px;">
</div>

iPQ-Check combina en todo momento la inspección 100 % de la impresión con la monitorización de banda de alta calidad a través de todo el formato de impresión. Campos de aplicación típicos: impresión de envases, impresiones decorativas e impresión de papel pintado y de etiquetas.

Con la línea de cámaras de alto rendimiento, iPQ-Check brinda información detallada del color de la imagen, para una lectura instantánea del patrón de impresión, comenzando desde la primera rotación del cilindro de impresión. Tecnologías y algoritmos avanzados aseguran una inspección completa y confiable y la inmediata eliminación de la fuente de defectos. El resultado: una calidad de impresión sin fallas que asombra a sus clientes.

</div>

<br><br><br>