+++
title = "Inspección de superficies"
description = ""
keywords = ["Soluciones","Inspección","Superficies","Bandas","Guias"]
+++

## Inspección avanzada de superficies para 100% de calidad

{{< figure src="/img/web-inspection-sol2-04.png" >}}
<br><br>

<h3><p style="color: mediumblue;">BST iPQ-Surface</p></h3> 

Es el **sistema fiable de inspección de superficies de alta gama** que se adapta totalmente a sus necesidades. Gracias a su **diseño modular**, el sistema ofrece siempre la mejor solución para su aplicación y situación de instalación.

La combinación de un hardware conectado en red y de alto rendimiento, un software intuitivo y estable, así como unos servicios globales de primera clase, le permiten tener tiempo para centrarse en lo más importante: su producción.

Además de los componentes de primera clase, tenemos más de 30 años de experiencia en la inspección de numerosos materiales. Gracias a este conocimiento de la aplicación y a nuestras **soluciones integradas**, se pueden evitar los defectos antes de que den lugar a problemas de calidad.

Con iPQ-Surface usted ofrece un 100% de calidad, aumenta su productividad y reduce sus residuos al mismo tiempo.
<br><br>

<h3><p style="color: mediumblue;">Aplicaciones más comunes: </p></h3>

* **Films plásticos**, transparentes y opácos
* **Papel**
* **Aluminio**
* **Film metalizados**
* **Non Wovens** (Papel Tissue)

<br><br>

<h3><p style="color: mediumblue;">El Hardware: 100% pensado hacia el futuro</h3></p>
<img src="/img/web-inspection-sol2-05.png" style="float: left; padding: 0 40px 0 0;">
<br><br>

<strong>Resultados perfectos</strong> necesitan una combinación exacta de <strong>captura de imagen e iluminación</strong>.iPQ-Surface tiene una integración inteligente de componentes.

* Cámaras de última generación para máxima resolución y velocidad.
* Un sistema de iluminación innovador hace todos los defectos visibles.
* Un concepto modular para adecuar la solución a cualquier instalación.

<br><br><br><br>

<h3><p style="color: mediumblue;">El Software: 100% confiable e intuitivo</h3></p>
<img src="/img/web-inspection-sol2-06.jpg" style="float: left; padding: 0 40px 0 0;">
<br><br>

Además de numerosas interfaces que facilitan la integración con otros sistemas, el software de iPQ-Surface tiene una excelente experiencia de usuario.

* Diseñado con precisión para el uso en ambientes de Industria 4.0.
* Una clasificación confiable de los defectos, basado en Inteligencia Artificial.
* Setup automático y operación intuitiva.
* Display claro, con los resultados de las mediciones y la localización de errores.

<br><br><br>