+++
title = "CLS CAM 100"
description = ""
keywords = ["Video","Youtube","Imágenes","CLS CAM 100"]
+++

{{< youtubehh  src="c3LUdGNC1os" title="CLS CAM 100" width="60%" height="400" >}}
<br>
> Nuevo sensor de BST modelo CLS CAM 100 para alinear por objetos. Es decir, en lugar de tener una línea preimpresa para alinear el material, este sensor usa un objeto de la impresión. Este objeto puede ser un logo, una letra o una parte de una imagen.
{.lead}