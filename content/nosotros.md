+++
title = "Nuestra Empresa"
description = "Nuestra Empresa"
keywords = ["Nosotros","Nuestra Empresa","Empresa"]
+++

{{< figure src="/img/trueimage_logo.png" width="25%" Title="TrueImage S.A." >}}
<br>
### ¿ Quienes Somos ?
> Somos una empresa con una larga trayectoria en el sector de impresión, tanto en envases flexibles, como banda angosta, y otros procesos de impresión. Hoy nos encontramos muy focalizados en soluciones con visión, que ayudan a reducir costos e incrementar la producción.<br><br>
Nuestra filosofía es brindar el mejor asesoramiento, la venta y el servicio técnico de sus productos.
{{< figure src="/img/nosotros.png" width="90%" Title="Productos" >}}

<br><br>
### ¿ Donde Estamos ?
> En Argentina, nos encontramos en el Parque industrial “La Cantábrica”, Haedo, Pcia. de Buenos Aires.<br><br>
La Cantábrica S.A. fue una gran empresa metalúrgica que inicio sus actividades el 12 de junio de 1902, continuando el trabajo que había comenzado la Fundición "El Carmen" con el primer tren laminador de acero instalado en el país en el año 1890.<br><br> 
Reconvertida a Parque Industrial hoy alberga, en un ámbito seguro y agradable, las empresas más importantes de la zona.
{{< figure src="/img/LaCantabrica.png" width="90%" Title="La Cantábrica">}}
{.lead .hlead}