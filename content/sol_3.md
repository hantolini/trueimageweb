+++
title = "Control de registros"
description = "Precisión para una calidad de impresión de primera"
keywords = ["Soluciones","Control","Registros"]
+++

## Precisión para una calidad de impresión de primera

{{< figure src="/img/web-register-contol-sol3-01.jpg" >}}
<br><br>

Los controles de registros son un componente importante de cualquier máquina de impresión. La monitorización automática y continuada del registro, cuando los colores de impresión se superponen, es esencial para garantizar una alta calidad en los resultados de impresión a largo plazo, especialmente en la impresión de banda. Los controles de registros aseguran que los colores individuales en la impresión polícroma confluyan de forma exacta y, así, que el resultado de color sea óptimo en cada impresión.

Los controles de registros de BST son el resultado de décadas de análisis de las necesidades de los usuarios. Por lo tanto, nuestro control de registros ha sido desarrollado para adaptarse a las necesidades de los campos de aplicación más diversos de la impresión de banda: **para impresión en huecograbado, flexográfica, offset y serigráfica, como también para los procesos en línea y fuera de línea**.

La última generación de controles de registros de BST están equipados para registrar marcas de impresión con **cabezales de escaneo de fibra óptica** o **cámaras matriciales CCD**. Estos garantizan la detección y medición completa de todas las marcas de impresión, incluso a altas velocidades.

Las marcas de impresión estandarizadas o creadas individualmente son capturadas de forma precisa en zonas definidas del lateral de la banda o en posiciones seleccionables libremente en la imagen impresa, independientemente de parámetros como el contraste, la disposición o la secuencia. Todo el manejo se lleva a cabo mediante un menú y todos los mensajes de estado aparecen en texto sin formato.
<br><br>

<h3><p style="color: mediumblue;">Control de registros de BST: Las ventajas que le ofrecemos</p></h3>

* **Tiempos de configuración** de encargos cortos.
* **Uso de material** mínimo debido a la mediumblueucción de deshechos en todas las fases de la impresión.
* Alta **calidad** constante debido a la coherencia del registro a lo largo de todo el proceso de impresión.
* Observancia exacta de todas las **tolerancias de registro**.
* **Productos impresos** de alta calidad para una mayor satisfacción del cliente.
<br>

<img src="/img/web-register-contol-sol3-05.png" style="float: left; padding: 0 20px 0 0;">

<br><br>
<div style="max-width: 800px;">
<h5><p style="color: mediumblue;">TWINStar: control de registro inovador</p></h5>
<strong>La competencia en la industria de la impresión es árdua</strong>, y las exigencias están en contínuo crecimiento.
El control preciso de la imágen a imprimir es clave para la excelencia de los productos impresos.<br><br>
TWINStar es un <strong>sistema de control de registro compacto</strong>, que permite administrar hasta dos estaciones de control.<br>
<strong>TWINStar es nuestra respuesta a los nuevos desafíos</strong>.<br><br>
Gracias a su arquitectura flexible se puede utilizar en equipos nuevos como en las líneas de máquinas ya existentes.<br><br>
<br><br><br><br>

||||
|---|---|---|
|<img src="/img/web-register-contol-sol3-02.jpg" width="270" height="140" style="margin: 10px 10px 10px 10px">|<img src="/img/web-register-contol-sol3-04.jpg" width="270" height="140" style="margin: 10px 10px 10px 10px">||
|<p style="color: mediumblue; margin: 0 0 0 10px;">**REGI_STAR 20**</p>|<p style="color: mediumblue; margin: 0 0 0 10px;">**AR 4100**</p>||

<br><br>