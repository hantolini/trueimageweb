+++
title = "Contacto"
id = "contact"
+++

# Estamos para ayudarlo

Si quiere saber más de nuestros productos y servicios, **¡ no dude en preguntarnos !**

Nuestro equipo de ventas lo asesorará en la mejor solución para su industria.

Nos encontrará de Lunes a Viernes, de 9 a 18hs.

>**Guillermo Costamagna**

Ventas: gcostamagna@trueimage.com.ar



